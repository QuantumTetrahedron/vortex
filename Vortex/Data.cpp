#include "Data.h"
#include "Log.h"

Data::Variables Data::variables;

void Data::Draw()
{
	static std::string selected = "";

	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 1.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8.0f, 8.0f));
	ImGui::Begin("Data", nullptr, ImGuiWindowFlags_NoMove);

	DrawVariableGroup<Components>("Components", selected);
	DrawVariableGroup<Image>("Images", selected);
	DrawVariableGroup<Int>("Ints", selected);
	DrawVariableGroup<Float>("Floats", selected);
	DrawVariableGroup<Bool>("Bools", selected);

	ImGui::End();
	ImGui::PopStyleVar(2);
}

void Data::ProcessIO(ImGuiIO& io)
{
}

bool Data::isValidVariableName(const char* name)
{
	if (std::strcmp(name, "") == 0) {
		Log::Error("Variable name can't be empty");
		return false;
	}
	if (variables.vars.contains(std::string(name))) {
		Log::Error("Variable name already in use");
		return false;
	}

	if (!((name[0] >= 'a' && name[0] <= 'z') || (name[0] >= 'A' && name[0] <= 'Z') || name[0] == '_')) {
		Log::Error("Variable name should start with a letter or an underscore");
		return false;
	}

	for (const char& ch : std::string(name)) {
		if (!((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == '_' || (ch >= '0' && ch <= '9'))) {
			Log::Error("Variable name not allowed");
			return false;
		}
	}
	return true;
}
