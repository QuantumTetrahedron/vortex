#pragma once
#include "Window.h"
#include <memory>
#include <set>
#include <type_traits>
#include <typeinfo>
#include <variant>
#include <functional>
#include "DataTypes.h"


class Data : public Window
{
public:
	struct Variables {
		using variant = std::variant<
			std::shared_ptr<Int>,
			std::shared_ptr<Float>,
			std::shared_ptr<Bool>,
			std::shared_ptr<Image>,
			std::shared_ptr<Components>
		>;
		std::map<std::string, variant> vars;

		template<typename T>
		std::shared_ptr<T> Get(const std::string& name) {
			return std::get<std::shared_ptr<T>>(vars.at(name));
		}

		template<typename T>
		bool IsOfType(const std::string& name) {
			if (auto value = std::get_if<std::shared_ptr<T>>(&vars.at(name))) {
				return true;
			}
			return false;
		}

		template<typename T>
		void Set(const std::string& name, const T& value) {
			vars[name] = std::make_shared<T>(value);
		}

		template<typename T>
		void Remove(const std::string& name) {
			vars.extract(name);
			NotifyOnRemove(name);
		}

		void RemoveAll() {
			vars.clear();
		}

		template<typename T>
		bool Empty() {
			return std::find_if(vars.begin(), vars.end(), [](auto& p) {return std::holds_alternative<std::shared_ptr<T>>(p.second); }) == vars.end();
		}

		void AddRemoveListener(std::function<void(const std::string& name)> fun) {
			removeListeners.push_back(fun);
		}
		void NotifyOnRemove(const std::string& name) {
			for (std::function<void(const std::string&)>& f : removeListeners) {
				f(name);
			}
		}
		std::vector<std::function<void(const std::string& name)>> removeListeners;
	};

	static Variables variables;

	void Draw() override;
	void ProcessIO(ImGuiIO& io) override;
private:

	bool isValidVariableName(const char* name);

	template<typename T>
	void DrawVariableGroup(const char* headerName, std::string& selected) {
		if (ImGui::CollapsingHeader(headerName)) {
			static char name[16] = "";
			std::string inputTextId = "##New";
			inputTextId.append(headerName);
			ImGui::InputText(inputTextId.c_str(), name, 16);

			ImGui::SameLine();
			std::string buttonId = "+##Add";
			buttonId.append(headerName);
			if (ImGui::Button(buttonId.c_str())) {
				if (isValidVariableName(name)) {
					variables.Set<T>(name, T(name));
				}
				name[0] = '\0';
			}

			std::set<std::string> toRemove;
			for (auto& p : variables.vars) {
				if (std::holds_alternative<std::shared_ptr<T>>(p.second)) {
					auto& name = p.first;
					std::string id = "-##Remove" + name;
					if (ImGui::Button(id.c_str())) {
						toRemove.insert(name);
					}
					ImGui::SameLine();
					if (ImGui::Selectable(name.c_str(), selected == name)) {
						selected = name;
						Properties::Display(variables.Get<T>(name));
					}
				}
			}

			for (const std::string& s : toRemove) {
				variables.Remove<T>(s);
			}
		}
	}
};

