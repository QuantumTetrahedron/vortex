#pragma once
#include "Properties.h"
#include "vk.h"
#include "Serializer.h"
#include <optional>

class Int : public IDisplayable, public ISerializable {
public:
	Int(const std::string& n) : name(n), value(0) {}

	void SetValue(int v) {
		value = v;
	}

	std::string name;

	void Display() override {
		std::string t = "Int ";
		t.append(name);
		ImGui::Text(t.c_str());
		ImGui::InputInt("default value", &value);
	}

	void Serialize(std::ofstream& out) {
		out << "int " << name << " = " << value << ";\n";
	}
private:
	int value;
};

class Float : public IDisplayable, public ISerializable {
public:
	Float(const std::string& n) : name(n), value(0.0f) {}

	void SetValue(float v) {
		value = v;
	}

	std::string name;

	void Display() override {
		std::string t = "Float ";
		t.append(name);
		ImGui::Text(t.c_str());
		ImGui::InputFloat("default value", &value);
	}

	void Serialize(std::ofstream& out) {
		out << "float " << name << " = " << value << ";\n";
	}

private:
	float value;
};

class Bool : public IDisplayable, public ISerializable {
public:
	Bool(const std::string& n) : name(n), value(false) {}
	
	void SetValue(bool v) {
		value = v;
		comboSelection = v ? 1 : 0;
	}
	
	std::string name;

	void Display() override {
		std::string t = "Bool ";
		t.append(name);
		ImGui::Text(t.c_str());

		const char* items[] = { "false", "true" };
		if (ImGui::Combo("default value", &comboSelection, items, 2)) {
			value = comboSelection == 1;
		}
	}

	void Serialize(std::ofstream& out) {
		out << "bool " << name << " = " << (value ? "true" : "false") << ";\n";
	}
private:
	bool value;
	int comboSelection = 0;
};

class Image : public IDisplayable, public ISerializable {
public:
	Image(const std::string& n) : name(n) {}

	void SetFormat(std::optional<int> f) {
		swapchainFormat = !f.has_value();
		format = f.value_or(0);
	}

	void SetSize(std::optional<glm::vec2> s) {
		swapchainSize = !s.has_value();
		resolution[0] = (int) s.value_or(glm::vec2(0.0f)).x;
		resolution[1] = (int) s.value_or(glm::vec2(0.0f)).y;
	}

	std::string name;

	void Display() override {
		std::string t = "Image ";
		t.append(name);
		ImGui::Text(t.c_str());

		ImGui::Separator();

		ImGui::Text("Format");
		ImGui::Checkbox("Swapchain format", &swapchainFormat);
		if (!swapchainFormat) {
			ImGui::Combo("##format", &format, vk::formats.data(), vk::formats.size());
		}

		ImGui::Separator();

		ImGui::Text("Resolution");
		ImGui::Checkbox("Swapchain size", &swapchainSize);
		if (!swapchainSize) {
			ImGui::InputInt2("##resolution", resolution);
		}

		ImGui::Separator();
	}

	void Serialize(std::ofstream& out) {
		out << "image " << name << "{\n";
		if (!swapchainFormat) {
			out << "\tformat = " << vk::formatsMap[vk::formats[format]] << ";\n";
		}
		if (!swapchainSize) {
			out << "\twidth = " << resolution[0] << ";\n\theight = " << resolution[1] << ";\n";
		}
		out << "}; \n";
	}

private:
	int format = 0;
	bool swapchainFormat = true;
	bool swapchainSize = true;
	int resolution[2] = { 0, 0 };
};

class Components : public IDisplayable, public ISerializable {
public:
	Components(const std::string& n) : name(n) {}
	std::string name;

	void Display() override {
		std::string t = "Components ";
		t.append(name);
		ImGui::Text(t.c_str());
	}

	void Serialize(std::ofstream& out) {
		out << "group " << name << ";\n";
	}
};