#include "FlowControl.h"

BeginNode::BeginNode(const std::string& _id) {
	id = _id;
	position = glm::vec2(0.0f);
	size = glm::vec2(150, 80);
	name = "S";

	canBeDeleted = false;

	AddSlot("next", "", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);
}

IfNode::IfNode(const std::string& _id)
{
	id = _id;
	position = glm::vec2(0.0f);
	size = glm::vec2(150, 80);
	name = "I";

	AddSlot("prev", "", Slot::Type::input, Slot::DataType::Exec, Slot::Quantity::Many);
	AddSlot("condition", "Cond.", Slot::Type::input, Slot::DataType::Bool, Slot::Quantity::One);
	AddSlot("true", "True", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);
	AddSlot("false", "False", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);
}

WhileNode::WhileNode(const std::string& _id)
{
	id = _id;
	position = glm::vec2(0.0f);
	size = glm::vec2(150, 80);
	name = "W";

	AddSlot("prev", "", Slot::Type::input, Slot::DataType::Exec, Slot::Quantity::Many);
	AddSlot("condition", "Cond.", Slot::Type::input, Slot::DataType::Bool, Slot::Quantity::One);
	AddSlot("do", "Do", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);
	AddSlot("then", "Then", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);
}