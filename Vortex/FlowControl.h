#pragma once
#include "Node.h"

class BeginNode : public Node {
public:
	BeginNode(const std::string& _id);
};

class IfNode : public Node {
public:
	IfNode(const std::string& _id);
};

class WhileNode : public Node {
public:
	WhileNode(const std::string& _id);
};
