#include "Graph.h"
#include "RenderPassExecNode.h"
#include <iostream>

std::shared_ptr<Graph> Graph::_inst;


std::vector<std::shared_ptr<Node>>& Graph::GetNodes()
{
	return nodes;
}

void Graph::RemoveNode(std::shared_ptr<Node> n)
{
	n->OnRemove();
	nodes.erase(
		std::remove(nodes.begin(), nodes.end(), n),
		nodes.end()
	);
}

void Graph::RemoveNodes(std::set<std::shared_ptr<Node>> ns)
{
	for (auto& n : ns) {
		n->OnRemove();
	}
	nodes.erase(
		std::remove_if(nodes.begin(), nodes.end(), [ns](std::shared_ptr<Node> n) { return ns.contains(n); }),
		nodes.end()
	);
}

std::shared_ptr<Graph> Graph::instance()
{
	if (!_inst) _inst = std::shared_ptr<Graph>(new Graph());
	return _inst;
}

void Graph::Serialize(std::ofstream& out)
{
	// render passes serialization

	// clear color / clear depth

	for (int i = 0; i < nodes.size(); i++) {
		nodes[i]->SetNum(i);
		nodes[i]->Serialize(out);
	}

	// nodes serialization

	out << "graph Vortex {\n";

	// nodes
	for (auto& n : nodes) {
		out << "\t" << n->GetID() << " " << n->GetMetadata() << ";\n";
	}
	// connections
	for (auto& n : nodes) {
		for (auto& slot : n->slots) {
			if (slot.second->GetType() == Slot::Type::output) {
				for (auto& connSlot : slot.second->connections) {
					const auto& conn = connSlot.lock()->GetParent();
					out << "\t" << n->GetNum() << ":" << slot.second->GetName() << "->" << conn.lock()->GetNum() << ":" << connSlot.lock()->GetName() << ";\n";
				}
			}
		}
	}

	out << "};\n";
}

void Graph::Clear()
{
	for (auto& n : nodes) {
		n->OnRemove();
	}

	nodes.clear();
}

Graph::Graph() {}