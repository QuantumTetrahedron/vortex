#pragma once

#include <concepts>
#include <utility>
#include "Data.h"

template <class Type>
concept IsNode = std::is_base_of<Node, Type>::value;

class Graph : public ISerializable {
public:
	std::vector<std::shared_ptr<Node>>& GetNodes();

	template<typename T> requires IsNode<T>
	std::shared_ptr<T> AddNode(std::string name, glm::vec2 at) {
		std::shared_ptr<T> newNode = std::make_shared<T>(name);
		newNode->position = at;
		newNode->Init();
		nodes.insert(nodes.begin(), newNode);
		return newNode;
	}

	void RemoveNode(std::shared_ptr<Node> n);
	void RemoveNodes(std::set<std::shared_ptr<Node>> ns);

	static std::shared_ptr<Graph> instance();

	void Serialize(std::ofstream& out);

	void Clear();
private:
	Graph();
	static std::shared_ptr<Graph> _inst;

	std::vector<std::shared_ptr<Node>> nodes;
};