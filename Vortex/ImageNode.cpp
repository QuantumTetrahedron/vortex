#include "ImageNode.h"
#include "vk.h"
#include "Data.h"

ImageNode::ImageNode(const std::string& imageName)
{
	image = Data::variables.Get<Image>(imageName);
	variableName = imageName;
	id = imageName;
	position = glm::vec2(0.0f);
	size = glm::vec2(150, 80);
	name = "GET." + imageName;

	AddSlot("write", "Write", Slot::Type::input, Slot::DataType::ImageWrite, Slot::Quantity::Many);
	AddSlot("read", "Read", Slot::Type::output, Slot::DataType::ImageRead, Slot::Quantity::Many);

	headerColor = GetSlot("read")->GetColor();
}

void ImageNode::OnClick()
{
	Properties::Display(image);
}

SwapchainNode::SwapchainNode(const std::string& _id)
{
	id = _id;
	position = glm::vec2(0.0f);
	size = glm::vec2(150, 80);
	name = "SC";

	canBeDeleted = true;

	AddSlot("write", "Write", Slot::Type::input, Slot::DataType::ImageWrite, Slot::Quantity::Many);
}
