#pragma once
#include "Node.h"
#include "Data.h"
#include "VarNode.h"

class ImageNode : public VarNode
{
public:
	ImageNode(const std::string& imageName);

	std::shared_ptr<Image> image;

	void OnClick() override;
};

class SwapchainNode : public Node 
{
public:
	SwapchainNode(const std::string& _id);
};
