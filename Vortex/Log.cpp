#include "Log.h"

std::deque<Log::Message> Log::messages;

void Log::Draw()
{
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 1.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8.0f, 8.0f));
    ImGui::Begin("Log", nullptr, ImGuiWindowFlags_NoMove);

    for (const Message& m : messages) {
        if (m.type == Message::Type::Error) {
            ImGui::TextColored(ImColor(0.8f, 0.2f, 0.2f), "Error: ");
        }
        else if (m.type == Message::Type::Warning) {
            ImGui::TextColored(ImColor(0.6f, 0.4f, 0.2f), "Warning: ");
        }
        else if (m.type == Message::Type::Info) {
            ImGui::TextColored(ImColor(0.2f, 0.2f, 0.6f), "Info: ");
        }

        ImGui::SameLine();
        ImGui::Text(m.message.c_str());
    }

    ImGui::End();
    ImGui::PopStyleVar(2);
}

void Log::ProcessIO(ImGuiIO& io)
{
}

void Log::Error(const std::string& message)
{
    messages.emplace_back(Message::Type::Error, message);
}

void Log::Warning(const std::string& message)
{
    messages.emplace_back(Message::Type::Warning, message);
}

void Log::Info(const std::string& message)
{
    messages.emplace_back(Message::Type::Info, message);
}

void Log::Clear()
{
    messages.clear();
}
