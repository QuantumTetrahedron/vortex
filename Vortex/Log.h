#pragma once
#include "Window.h"
#include <deque>

class Log : public Window
{
public:
	void Draw() override;
	void ProcessIO(ImGuiIO& io) override;

	static void Error(const std::string& message);
	static void Warning(const std::string& message);
	static void Info(const std::string& message);
	static void Clear();
private:
	struct Message {
		enum class Type {
			Error, Warning, Info
		};

		Message(Type t, const std::string& m) : type(t), message(m) {}
		Type type;
		std::string message;
	};

	static std::deque<Message> messages;
};

