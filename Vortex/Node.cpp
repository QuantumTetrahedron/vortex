
#include "NodesRenderer.h"
#include "Node.h"
#include "Window.h"

void Node::Draw(ImDrawList* drawList, glm::vec2 positionOffset)
{
	ndraw::NodeDrawConfig cfg;
	cfg.position = position + positionOffset;
	cfg.size = size;
	cfg.backgroundColor = backgroundColor;
	cfg.borderColor = ImColor(0.0f, 0.0f, 0.0f);
	cfg.headerColor = headerColor;

	std::shared_ptr<Node> t = shared_from_this();
	if (Window::IsSelected(t)) {
		cfg.highlightColor = ImColor(0.8f, 0.8f, 0.4f);
	}
	else if (Window::IsHovered(t)) {
		cfg.highlightColor = ImColor(0.4f, 0.4f, 0.8f);
	}

	cfg.label.position = position + positionOffset + glm::vec2(5.0f);
	cfg.label.color = ImColor(0.0f, 0.0f, 0.0f);
	cfg.label.size = 18;
	cfg.label.text = id;

	ndraw::DrawNode(drawList, cfg);

	for (auto& s : slots) {
		s.second->Draw(drawList, positionOffset + position);
	}
}

void Node::DrawConnections(ImDrawList* drawList, glm::vec2 positionOffset)
{
	for (auto& p : slots) {
		auto s = p.second;
		s->DrawConnections(drawList, positionOffset);
	}
}

std::shared_ptr<Slot> Node::MouseOverSlot(glm::vec2 mousePos)
{
	for (auto& p : slots) {
		auto s = p.second;
		glm::vec2 slotPos = s->GetPosition();
		if (glm::length(slotPos - mousePos) < 10.0f) {
			return s;
		}
	}
	return nullptr;
}

bool Node::MouseOverNode(glm::vec2 mousePos) {
	if (position.x < mousePos.x && position.x + size.x > mousePos.x) {
		if (position.y < mousePos.y && position.y + size.y > mousePos.y) {
			return true;
		}
	}
	return false;
}

void Node::OnRemove()
{
	for (auto& p : slots) {
		p.second->DisconnectAll();
	}
}

std::shared_ptr<Slot> Node::GetSlot(const std::string& name) const
{
	for (auto p : slots) {
		if (p.first == name) {
			return p.second;
		}
	}
	return nullptr;
}

void Node::AddSlot(const std::string& id, const std::string& dispName, Slot::Type type, Slot::DataType dataType, Slot::Quantity quantity)
{
	glm::vec2 lowerPos = glm::vec2(type == Slot::Type::input ? 15 : size.x - 15, 15 + slotOffset);
	for (auto& p : slots) {
		std::shared_ptr<Slot> s = p.second;
		float y = s->GetPosition(false).y + 25;
		if (s->GetType() == type && y > lowerPos.y) {
			lowerPos.y = y;
		}
	}

	std::shared_ptr<Slot> s = std::make_shared<Slot>(id, dispName, type, dataType, quantity, lowerPos);

	slots.push_back(std::make_pair(id, s));
}
