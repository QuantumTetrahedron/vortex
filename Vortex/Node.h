#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <map>
#include <string>
#include <memory>

#include "ImGUI/imgui.h"
#include "ImGUI/imgui_impl_glfw.h"
#include "ImGUI/imgui_impl_vulkan.h"
#include "ImGUI/imgui_internal.h"

#include "Slot.h"
#include "Serializer.h"

class Node : public std::enable_shared_from_this<Node>, public ISerializable
{
public:
	virtual void Draw(ImDrawList* drawList, glm::vec2 positionOffset);
	void DrawConnections(ImDrawList* drawList, glm::vec2 positionOffset);
	std::shared_ptr<Slot> MouseOverSlot(glm::vec2 mousePos);
	bool MouseOverNode(glm::vec2 mousePos);
	
	virtual void OnRemove();

	void SetNum(int n) {
		num = n;
	}

	int GetNum() const {
		return num;
	}

	std::string GetID() {
		return name + "." + std::to_string(num);
	}

	virtual std::string GetMetadata() {
		std::string out = "[";
		out += "x=" + std::to_string((int) position.x);
		out += ",y=" + std::to_string((int) position.y);
		out += "]";
		return out;
	}

	std::shared_ptr<Slot> GetSlot(const std::string& name) const;
	std::vector<std::pair<std::string, std::shared_ptr<Slot>>> slots;

	glm::vec2 position;
	glm::vec2 size;

	bool canBeDeleted = true;

	virtual void OnClick() {}
	void Init() {
		for (auto& p : slots) {
			auto& s = p.second;
			s->SetParent(shared_from_this());
		}
	}

	void Serialize(std::ofstream& out) {}

protected:
	Node() = default;
	virtual void AddSlot(const std::string& id, const std::string& dispName, Slot::Type type, Slot::DataType dataType, Slot::Quantity quantity);
	std::string id;
	ImColor headerColor = ImColor(0.4f, 0.4f, 0.4f, 0.8f);
	ImColor backgroundColor = ImColor(0.75f, 0.75f, 0.75f, 0.8f);
	float slotOffset = 25.0f;

	int num;
	std::string name;
};

