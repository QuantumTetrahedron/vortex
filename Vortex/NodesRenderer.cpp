#include "NodesRenderer.h"

void ndraw::DrawNode(ImDrawList* drawList, const NodeDrawConfig& cfg)
{
	ImVec2 a(cfg.position.x, cfg.position.y);
	ImVec2 b(a.x + cfg.size.x, a.y + 25.0f);
	ImVec2 c(a.x, b.y);
	ImVec2 d(a.x + cfg.size.x, a.y + cfg.size.y);

	ImColor bgColor = cfg.backgroundColor.value_or(ImColor(0.75f, 0.75f, 0.75f, 0.8f));

	if (cfg.headerColor.has_value()) {
		drawList->AddRectFilled(a, b, cfg.headerColor.value(), 10.0f, ImDrawCornerFlags_Top);
		drawList->AddRectFilled(c, d, bgColor, 10.0f, ImDrawCornerFlags_Bot);

		if (cfg.borderColor.has_value()) {
			drawList->AddRect(a, b, cfg.borderColor.value(), 10.0f, ImDrawCornerFlags_Top, 1.0f);
			drawList->AddRect(a, d, cfg.borderColor.value(), 10.0f, ImDrawCornerFlags_All, 5.0f);
		}
	}
	else {
		drawList->AddRectFilled(a, d, bgColor, 10.0f, ImDrawCornerFlags_All);

		if (cfg.borderColor.has_value()) {
			drawList->AddRect(a, d, cfg.borderColor.value(), 10.0f, ImDrawCornerFlags_All, 5.0f);
		}
	}

	if (cfg.highlightColor.has_value()) {
		ImVec2 a1, a2, b1, b2;
		a1 = { a.x - 2.0f, a.y - 2.0f };
		b1 = { d.x + 2.0f, d.y + 2.0f };
		a2 = { a.x + 2.0f, a.y + 2.0f };
		b2 = { d.x - 2.0f, d.y - 2.0f };
		drawList->AddRect(a1, b1, cfg.highlightColor.value(), 10.0f);
		drawList->AddRect(a2, b2, cfg.highlightColor.value(), 10.0f);
	}

	ImVec2 textPos{ cfg.label.position.x, cfg.label.position.y };
	drawList->AddText(ImGui::GetDefaultFont(), cfg.label.size, textPos, cfg.label.color, cfg.label.text.c_str(), (const char*)0, cfg.size.x - 5.0f);
}

void ndraw::DrawSlot(ImDrawList* drawList, const SlotDrawConfig& cfg)
{
	drawList->AddCircleFilled(ImVec2(cfg.position.x, cfg.position.y), cfg.radius, cfg.backgroundColor);

	if (cfg.highlightColor.has_value()) {
		drawList->AddRectFilled(ImVec2(cfg.position.x - cfg.radius, cfg.position.y - cfg.radius),
								ImVec2(cfg.position.x + cfg.radius, cfg.position.y + cfg.radius),
								cfg.highlightColor.value());
	}

	if (cfg.lineColor.has_value()) {
		drawList->AddCircleFilled(ImVec2(cfg.position.x, cfg.position.y), cfg.radius - 4.0f, cfg.lineColor.value());
	}

	drawList->AddCircle(ImVec2(cfg.position.x, cfg.position.y), cfg.radius, cfg.outlineColor, 12, 2.0f);

	if (cfg.label.show) {
		std::string text = cfg.label.text;
		float textOffset = cfg.label.offset;
		ImVec2 textPos = ImVec2(cfg.position.x + textOffset, cfg.position.y - cfg.label.size / 2.0f);
		drawList->AddText(ImGui::GetDefaultFont(), cfg.label.size, textPos, cfg.label.color, cfg.label.text.c_str());
	}
}

void ndraw::DrawConnection(ImDrawList* drawList, const ConnectionDrawConfig& cfg)
{
	ImVec2 origin(cfg.from.x, cfg.from.y);
	ImVec2 target(cfg.to.x, cfg.to.y);

	float d = (target.x - origin.x) / 2.0f;
	float dx = glm::max(glm::abs(d), glm::min(100.0f, glm::length(cfg.to - cfg.from)));

	ImVec2 p1(origin.x + dx, origin.y);
	ImVec2 p2(target.x - dx, target.y);

	drawList->AddBezierCurve(origin, p1, p2, target, cfg.color, cfg.thickness);
}
