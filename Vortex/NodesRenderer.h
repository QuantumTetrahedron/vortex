#pragma once

#include <glm/glm.hpp>
#include <optional>
#include <string>
#include "ImGUI/imgui.h"
#include "ImGUI/imgui_internal.h"

namespace ndraw {

	struct NodeDrawConfig {
		glm::vec2 position;
		glm::vec2 size;

		std::optional<ImColor> borderColor;
		std::optional<ImColor> headerColor;
		std::optional<ImColor> highlightColor;
		std::optional<ImColor> backgroundColor;

		struct {
			std::string text;
			float size;
			ImColor color;
			glm::vec2 position;
		} label;
	};

	struct SlotDrawConfig {
		glm::vec2 position;
		float radius;
		ImColor backgroundColor;
		ImColor outlineColor;
		std::optional<ImColor> lineColor;
		std::optional<ImColor> highlightColor;

		struct {
			bool show;
			std::string text;
			float size;
			float offset;
			ImColor color;
		} label;
	};

	struct ConnectionDrawConfig {
		glm::vec2 from;
		glm::vec2 to;
		ImColor color;
		float thickness;
	};

	void DrawNode(ImDrawList* drawList, const NodeDrawConfig& cfg);
	void DrawSlot(ImDrawList* drawList, const SlotDrawConfig& cfg);
	void DrawConnection(ImDrawList* drawList, const ConnectionDrawConfig& cfg);

}