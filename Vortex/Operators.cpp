#include "Operators.h"
#include "NodesRenderer.h"

OperatorNode::OperatorNode()
{
	slotOffset = 0.0f;
}

void OperatorNode::Draw(ImDrawList* drawList, glm::vec2 positionOffset)
{
	ndraw::NodeDrawConfig cfg;
	cfg.position = position + positionOffset;
	cfg.size = size;
	cfg.backgroundColor = backgroundColor;
	cfg.borderColor = ImColor(0.0f, 0.0f, 0.0f);

	std::shared_ptr<Node> t = shared_from_this();
	if (Window::IsSelected(t)) {
		cfg.highlightColor = ImColor(0.8f, 0.8f, 0.4f);
	}
	else if (Window::IsHovered(t)) {
		cfg.highlightColor = ImColor(0.4f, 0.4f, 0.8f);
	}

	glm::vec2 pos = position + positionOffset + size / glm::vec2(2.0f);
	pos -= glm::vec2(5 * id.length(), 10.0f);
	cfg.label.position = pos;
	cfg.label.color = ImColor(0.0f, 0.0f, 0.0f);
	cfg.label.size = 18;
	cfg.label.text = id;

	ndraw::DrawNode(drawList, cfg);

	for (auto& p : slots) {
		auto s = p.second;
		s->Draw(drawList, positionOffset + position);
	}
}