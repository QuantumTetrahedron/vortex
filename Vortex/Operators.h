#pragma once
#include "Node.h"
#include "Window.h"

class OperatorNode : public Node {
public:
	OperatorNode();
	void Draw(ImDrawList* drawList, glm::vec2 positionOffset) override;
};

template<Slot::DataType t1, Slot::DataType t2> std::string getVariant() {
	if (t1 == Slot::DataType::Int && t2 == Slot::DataType::Int) {
		return "0";
	}
	if (t1 == Slot::DataType::Int && t2 == Slot::DataType::Float) {
		return "1";
	}
	if (t1 == Slot::DataType::Float && t2 == Slot::DataType::Int) {
		return "2";
	}
	if (t1 == Slot::DataType::Float && t2 == Slot::DataType::Float) {
		return "3";
	}
}

template<Slot::DataType t1, Slot::DataType t2>
class EqualNode : public OperatorNode {
public:
	EqualNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.eq." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class NotEqualNode : public OperatorNode {
public:
	NotEqualNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.neq." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class GreaterNode : public OperatorNode {
public:
	GreaterNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.gt." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class GreaterEqualNode : public OperatorNode {
public:
	GreaterEqualNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.geq." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class LesserNode : public OperatorNode {
public:
	LesserNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.lt." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class LesserEqualNode : public OperatorNode {
public:
	LesserEqualNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.leq." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class AddNode : public OperatorNode {
public:
	AddNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.add." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		auto outType = t1 == Slot::DataType::Float || t2 == Slot::DataType::Float ? Slot::DataType::Float : Slot::DataType::Int;
		AddSlot("out", "", Slot::Type::output, outType, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class SubtractNode : public OperatorNode {
public:
	SubtractNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.sub." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		auto outType = t1 == Slot::DataType::Float || t2 == Slot::DataType::Float ? Slot::DataType::Float : Slot::DataType::Int;
		AddSlot("out", "", Slot::Type::output, outType, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class MultiplyNode : public OperatorNode {
public:
	MultiplyNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.mul." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		auto outType = t1 == Slot::DataType::Float || t2 == Slot::DataType::Float ? Slot::DataType::Float : Slot::DataType::Int;
		AddSlot("out", "", Slot::Type::output, outType, Slot::Quantity::Many);
	}
};

template<Slot::DataType t1, Slot::DataType t2>
class DivideNode : public OperatorNode {
public:
	DivideNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.div." + getVariant<t1, t2>();
		AddSlot("v1", "", Slot::Type::input, t1, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, t2, Slot::Quantity::One);
		auto outType = t1 == Slot::DataType::Float || t2 == Slot::DataType::Float ? Slot::DataType::Float : Slot::DataType::Int;
		AddSlot("out", "", Slot::Type::output, outType, Slot::Quantity::Many);
	}
};

class AndNode : public OperatorNode {
public:
	AndNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.and";
		AddSlot("v1", "", Slot::Type::input, Slot::DataType::Bool, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, Slot::DataType::Bool, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

class OrNode : public OperatorNode {
public:
	OrNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 55);
		name = "O.or";
		AddSlot("v1", "", Slot::Type::input, Slot::DataType::Bool, Slot::Quantity::One);
		AddSlot("v2", "", Slot::Type::input, Slot::DataType::Bool, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

class NotNode : public OperatorNode {
public:
	NotNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(80, 30);
		name = "O.not";
		AddSlot("v1", "", Slot::Type::input, Slot::DataType::Bool, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, Slot::DataType::Bool, Slot::Quantity::Many);
	}
};

template<Slot::DataType type>
concept selectable = type != Slot::DataType::Exec && type != Slot::DataType::ImageWrite;

template<Slot::DataType type> requires selectable<type>
class SelectNode : public Node {
public:
	SelectNode(const std::string& _id) {
		id = _id;
		position = glm::vec2(0.0f);
		size = glm::vec2(150, 160);
		name = "SEL.";

		if (type == Slot::DataType::Int) name += "int";
		if (type == Slot::DataType::Float) name += "float";
		if (type == Slot::DataType::Bool) name += "bool";
		if (type == Slot::DataType::ImageRead) name += "image";
		if (type == Slot::DataType::RenderComponents) name += "group";

		AddSlot("choice", "Choice", Slot::Type::input, Slot::DataType::Int, Slot::Quantity::One);
		AddSlot("opt1", "1", Slot::Type::input, type, Slot::Quantity::One);
		AddSlot("opt2", "2", Slot::Type::input, type, Slot::Quantity::One);
		AddSlot("opt3", "3", Slot::Type::input, type, Slot::Quantity::One);
		AddSlot("opt4", "4", Slot::Type::input, type, Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, type, Slot::Quantity::Many);
	}
};
