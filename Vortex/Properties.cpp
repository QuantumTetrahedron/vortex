#include "Properties.h"

std::shared_ptr<IDisplayable> Properties::displayed;

Properties::Properties()
{
    displayed = nullptr;
}

void Properties::Draw() {
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 1.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8.0f, 8.0f));
    ImGui::Begin("Properties", nullptr, ImGuiWindowFlags_NoMove);

    if (displayed) {
        displayed->Display();
    }
    else {
        ImGui::Text("No node selected");
    }

    ImGui::End();
    ImGui::PopStyleVar(2);
}

void Properties::ProcessIO(ImGuiIO& io) {

}