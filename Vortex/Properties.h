#pragma once
#include "Window.h"
#include <variant>
#include <memory>

class IDisplayable {
public:
	virtual void Display() = 0;
};

class Properties : public Window
{
public:
	Properties();
	void Draw() override;
	void ProcessIO(ImGuiIO& io) override;

	static void Display(std::shared_ptr<IDisplayable> toDisplay) {
		displayed = toDisplay;
	}

private:
	static std::shared_ptr<IDisplayable> displayed;
};

