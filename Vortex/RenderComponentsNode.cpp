#include "RenderComponentsNode.h"


RenderComponentsNode::RenderComponentsNode(const std::string& n)
{
	id = n;
	variableName = n;
	position = glm::vec2(0.0f);
	size = glm::vec2(120, 60);
	name = "GET." + n;

	components = Data::variables.Get<Components>(n);

	AddSlot("components", "", Slot::Type::output, Slot::DataType::RenderComponents, Slot::Quantity::Many);
	headerColor = GetSlot("components")->GetColor();
}

void RenderComponentsNode::OnClick()
{
	Properties::Display(components);
}
