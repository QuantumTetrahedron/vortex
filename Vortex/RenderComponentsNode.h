#pragma once
#include "Node.h"
#include "Data.h"
#include "VarNode.h"

class RenderComponentsNode : public VarNode
{
public:
	RenderComponentsNode(const std::string& id);

	void OnClick() override;
private:
	std::shared_ptr<Components> components;
};

