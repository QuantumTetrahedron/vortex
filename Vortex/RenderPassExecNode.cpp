#include "RenderPassExecNode.h"
#include "Properties.h"
#include "NodesRenderer.h"

RenderPassExecNode::RenderPassExecNode(const std::string& _id)
{
	id = _id;
	position = glm::vec2(0, 0);
	size = glm::vec2(200, 110);
	name = "RP";

	data = std::make_shared<RenderPassData>();
	data->SetShaderName("");
	data->isPostprocess = false;
	data->inputsCount = 1;
	data->outputsCount = 1;

	AddSlot("prev", "", Slot::Type::input, Slot::DataType::Exec, Slot::Quantity::Many);
	AddSlot("next", "", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);

	AddSlot("groups", "Components", Slot::Type::input, Slot::DataType::RenderComponents, Slot::Quantity::Many);

	AddSlot("target1", "Target 1", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
	AddSlot("target2", "Target 2", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
	AddSlot("target3", "Target 3", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
	AddSlot("target4", "Target 4", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
}

void RenderPassExecNode::OnClick()
{
	Properties::Display(data);
}

void RenderPassExecNode::Serialize(std::ofstream& out)
{
	bool clearColorEnabled = data->GetClearColorEnabled();
	bool clearDepthEnabled = data->GetClearDepthEnabled();

	out << "renderpass " << num << "{\n";
	out << "\tclearColorEnabled = " << (clearColorEnabled ? "true" : "false") << ";\n";
	if (clearColorEnabled) {
		glm::vec3 clearColorValue = data->GetClearColorValue();
		out << "\tclearColorValue = " << clearColorValue.r << " " << clearColorValue.g << " " << clearColorValue.b << ";\n";
	}
	out << "\tclearDepthEnabled = " << (clearDepthEnabled ? "true" : "false") << ";\n";
	if (clearDepthEnabled) {
		out << "\tclearDepthValue = " << data->GetClearDepthValue() << ";\n";
	}
	out << "\tshader = " << data->GetShaderName() << ";\n";
	out << "\tisPostprocess = " << (data->isPostprocess ? "true" : "false") << ";\n";
	out << "};\n";
}

void RenderPassExecNode::Draw(ImDrawList* drawList, glm::vec2 positionOffset)
{
	size = glm::vec2(200, 35);
	int inputsCount = 2;
	int outputsCount = data->outputsCount + 1;

	size.y += 25 * glm::max(inputsCount, outputsCount);

	ndraw::NodeDrawConfig cfg;
	cfg.position = position + positionOffset;
	cfg.size = size;
	cfg.backgroundColor = backgroundColor;
	cfg.borderColor = ImColor(0.0f, 0.0f, 0.0f);
	cfg.headerColor = headerColor;

	std::shared_ptr<Node> t = shared_from_this();
	if (Window::IsSelected(t)) {
		cfg.highlightColor = ImColor(0.8f, 0.8f, 0.4f);
	}
	else if (Window::IsHovered(t)) {
		cfg.highlightColor = ImColor(0.4f, 0.4f, 0.8f);
	}

	cfg.label.position = position + positionOffset + glm::vec2(5.0f);
	cfg.label.color = ImColor(0.0f, 0.0f, 0.0f);
	cfg.label.size = 18;
	cfg.label.text = id;

	ndraw::DrawNode(drawList, cfg);

	std::vector<std::shared_ptr<Slot>> inputs;
	std::vector<std::shared_ptr<Slot>> outputs;

	for (auto& s : slots) {
		if (s.second->GetType() == Slot::Type::input) {
			inputs.push_back(s.second);
		}
		else {
			outputs.push_back(s.second);
		}
	}

	for (int i = 0; i < inputs.size(); ++i) {
		if (i < inputsCount) {
			inputs[i]->Draw(drawList, positionOffset + position);
		}
		else {
			inputs[i]->DisconnectAll();
		}
	}

	for (int i = 0; i < outputs.size(); ++i) {
		if (i < outputsCount) {
			outputs[i]->Draw(drawList, positionOffset + position);
		}
		else {
			outputs[i]->DisconnectAll();
		}
	}
}

std::string RenderPassExecNode::GetMetadata()
{
	std::string out = "[";
	out += "x=" + std::to_string((int) position.x);
	out += ",y=" + std::to_string((int) position.y);
	out += ",in=" + std::to_string(data->inputsCount);
	out += ",out=" + std::to_string(data->outputsCount);
	out += "]";
	return out;
}

void RenderPassData::Display()
{
	std::string t = "Render Pass";

	ImGui::Text(t.c_str());

	const char* items[] = { "false", "true" };
	if (ImGui::Combo("Clear Color", &cc_selection, items, 2)) {
		clearColorEnabled = cc_selection == 1;
	}
	if (clearColorEnabled) {
		ImGui::ColorEdit3("##Clear Color Value", &clearColorValue[0], 0);
	}

	if (ImGui::Combo("Clear Depth", &cd_selection, items, 2)) {
		clearDepthEnabled = cd_selection == 1;
	}
	if (clearDepthEnabled) {
		ImGui::InputFloat("##Clear Depth Value", &clearDepthValue);
	}

	ImGui::InputText("Shader Name", name, 256);

	shaderName = std::string(name);

	if (isPostprocess) {
		ImGui::SliderInt("inputs count", &inputsCount, 1, 4);
	}

	ImGui::SliderInt("outputs count", &outputsCount, 1, 4);
}

PostprocessExecNode::PostprocessExecNode(const std::string& _id)
{
	id = _id;
	position = glm::vec2(0, 0);
	size = glm::vec2(200, 35);
	name = "RP";

	data = std::make_shared<RenderPassData>();
	data->SetShaderName("");
	data->isPostprocess = true;
	data->inputsCount = 1;
	data->outputsCount = 1;

	AddSlot("prev", "", Slot::Type::input, Slot::DataType::Exec, Slot::Quantity::Many);
	AddSlot("next", "", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);

	AddSlot("inputImage1", "Image1", Slot::Type::input, Slot::DataType::ImageRead, Slot::Quantity::One);
	AddSlot("inputImage2", "Image2", Slot::Type::input, Slot::DataType::ImageRead, Slot::Quantity::One);
	AddSlot("inputImage3", "Image3", Slot::Type::input, Slot::DataType::ImageRead, Slot::Quantity::One);
	AddSlot("inputImage4", "Image4", Slot::Type::input, Slot::DataType::ImageRead, Slot::Quantity::One);

	AddSlot("target1", "Target 1", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
	AddSlot("target2", "Target 2", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
	AddSlot("target3", "Target 3", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
	AddSlot("target4", "Target 4", Slot::Type::output, Slot::DataType::ImageWrite, Slot::Quantity::One);
}

void PostprocessExecNode::OnClick()
{
	Properties::Display(data);
}

void PostprocessExecNode::Serialize(std::ofstream& out)
{
	bool clearColorEnabled = data->GetClearColorEnabled();
	bool clearDepthEnabled = data->GetClearDepthEnabled();

	out << "renderpass " << num << "{\n"; 
	out << "\tclearColorEnabled = " << (clearColorEnabled ? "true" : "false") << ";\n";
	if (clearColorEnabled) {
		glm::vec3 clearColorValue = data->GetClearColorValue();
		out << "\tclearColorValue = " << clearColorValue.r << " " << clearColorValue.g << " " << clearColorValue.b << ";\n";
	}
	out << "\tclearDepthEnabled = " << (clearDepthEnabled ? "true" : "false") << ";\n";
	if (clearDepthEnabled) {
		out << "\tclearDepthValue = " << data->GetClearDepthValue() << ";\n";
	}
	out << "\tshader = " << data->GetShaderName() << ";\n";
	out << "\tisPostprocess = " << (data->isPostprocess ? "true" : "false") << ";\n";
	out << "};\n";
}

void PostprocessExecNode::Draw(ImDrawList* drawList, glm::vec2 positionOffset)
{
	size = glm::vec2(200, 35);
	int inputsCount = data->inputsCount + 1;
	int outputsCount = data->outputsCount + 1;

	size.y += 25 * glm::max(inputsCount, outputsCount);

	ndraw::NodeDrawConfig cfg;
	cfg.position = position + positionOffset;
	cfg.size = size;
	cfg.backgroundColor = backgroundColor;
	cfg.borderColor = ImColor(0.0f, 0.0f, 0.0f);
	cfg.headerColor = headerColor;

	std::shared_ptr<Node> t = shared_from_this();
	if (Window::IsSelected(t)) {
		cfg.highlightColor = ImColor(0.8f, 0.8f, 0.4f);
	}
	else if (Window::IsHovered(t)) {
		cfg.highlightColor = ImColor(0.4f, 0.4f, 0.8f);
	}

	cfg.label.position = position + positionOffset + glm::vec2(5.0f);
	cfg.label.color = ImColor(0.0f, 0.0f, 0.0f);
	cfg.label.size = 18;
	cfg.label.text = id;

	ndraw::DrawNode(drawList, cfg);

	std::vector<std::shared_ptr<Slot>> inputs;
	std::vector<std::shared_ptr<Slot>> outputs;

	for (auto& s : slots) {
		if (s.second->GetType() == Slot::Type::input) {
			inputs.push_back(s.second);
		}
		else {
			outputs.push_back(s.second);
		}
	}

	for (int i = 0; i < inputs.size(); ++i) {
		if (i < inputsCount) {
			inputs[i]->Draw(drawList, positionOffset + position);
		}
		else {
			inputs[i]->DisconnectAll();
		}
	}

	for (int i = 0; i < outputs.size(); ++i) {
		if (i < outputsCount) {
			outputs[i]->Draw(drawList, positionOffset + position);
		}
		else {
			outputs[i]->DisconnectAll();
		}
	}
}

std::string PostprocessExecNode::GetMetadata()
{
	std::string out = "[";
	out += "x=" + std::to_string((int) position.x);
	out += ",y=" + std::to_string((int) position.y);
	out += ",in=" + std::to_string(data->inputsCount);
	out += ",out=" + std::to_string(data->outputsCount);
	out += "]";
	return out;
}
