#pragma once
#include "Node.h"
#include "Properties.h"

class RenderPassData : public IDisplayable {
public:
	void SetClearColorEnabled(bool v) {
		clearColorEnabled = v;
		cc_selection = v ? 1 : 0;
	}

	void SetClearDepthEnabled(bool v) {
		clearDepthEnabled = v;
		cd_selection = v ? 1 : 0;
	}

	void SetClearColorValue(glm::vec3 color) {
		clearColorValue = color;
	}

	void SetClearDepthValue(float value) {
		clearDepthValue = value;
	}

	void SetShaderName(const std::string& s) {
		shaderName = s;
		size_t len = s.length() > 255 ? 255 : s.length();
		for (int i = 0; i < len; ++i) {
			name[i] = s[i];
		}
		name[len] = '\0';
	}

	bool GetClearColorEnabled() { return clearColorEnabled; }
	glm::vec3 GetClearColorValue() { return clearColorValue; }
	bool GetClearDepthEnabled() { return clearDepthEnabled; }
	float GetClearDepthValue() { return clearDepthValue; }
	std::string GetShaderName() { return shaderName; }

	int outputsCount;

	bool isPostprocess;
	int inputsCount;

	void Display() override;
private:

	bool clearColorEnabled;
	glm::vec3 clearColorValue;
	bool clearDepthEnabled;
	float clearDepthValue;
	std::string shaderName;

	int cc_selection, cd_selection;
	char name[256] = "\0";
};

class RenderPassExecNode : public Node
{
public:
	RenderPassExecNode(const std::string& id);
	void OnClick() override;
	void Serialize(std::ofstream& out) override;
	void Draw(ImDrawList* drawList, glm::vec2 positionOffset) override;
	std::string GetMetadata() override;

	std::shared_ptr<RenderPassData> data;
};

class PostprocessExecNode : public Node 
{
public:
	PostprocessExecNode(const std::string& id);

	void OnClick() override;
	void Serialize(std::ofstream& out) override;
	void Draw(ImDrawList* drawList, glm::vec2 positionOffset) override;
	std::string GetMetadata() override;

	std::shared_ptr<RenderPassData> data;
};
