#include "Serializer.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <variant>
#include <sstream>
#include "Data.h"
#include "Graph.h"
#include "RenderPassExecNode.h"
#include "FlowControl.h"
#include "ImageNode.h"
#include "Operators.h"
#include "RenderComponentsNode.h"

struct Serializer::pImpl {
    std::map<uint32_t, std::shared_ptr<RenderPassData>> renderPassesCache;
    std::map<uint32_t, std::shared_ptr<Node>> nodesCache;

    template<typename T> requires IsNode<T>
    std::shared_ptr<T> addNode(uint32_t id, const std::string& name, glm::vec2 pos) {
        auto node = Graph::instance()->AddNode<T>(name, pos);
        nodesCache[id] = node;
        return node;
    }

    std::shared_ptr<Node> getNode(uint32_t id) {
        return nodesCache[id];
    }

    template<Slot::DataType t1, Slot::DataType t2>
    void ParseOperatorNode(uint32_t id, const std::string& operatorCode, glm::vec2 pos) {
        if (operatorCode == "add") {
            addNode<AddNode<t1, t2>>(id, "+", pos);
        }
        if (operatorCode == "sub") {
            addNode<SubtractNode<t1, t2>>(id, "-", pos);
        }
        if (operatorCode == "mul") {
            addNode<MultiplyNode<t1, t2>>(id, "*", pos);
        }
        if (operatorCode == "div") {
            addNode<DivideNode<t1, t2>>(id, "/", pos);
        }
        if (operatorCode == "eq") {
            addNode<EqualNode<t1, t2>>(id, "==", pos);
        }
        if (operatorCode == "neq") {
            addNode<NotEqualNode<t1, t2>>(id, "!=", pos);
        }
        if (operatorCode == "gt") {
            addNode<GreaterNode<t1, t2>>(id, ">", pos);
        }
        if (operatorCode == "geq") {
            addNode<GreaterEqualNode<t1, t2>>(id, ">=", pos);
        }
        if (operatorCode == "lt") {
            addNode<LesserNode<t1, t2>>(id, "<", pos);
        }
        if (operatorCode == "leq") {
            addNode<LesserEqualNode<t1, t2>>(id, "<=", pos);
        }
    }
};

Serializer::Serializer()
{
    impl = new pImpl();
}

Serializer::~Serializer()
{
    delete impl;
}

void Serializer::Serialize(const char* file)
{
    std::cout << "Saving to file: " << file << std::endl;

    std::ofstream f(file, std::ios::trunc | std::ios::out);

    if (!f.is_open()) {
        std::cerr << "Saving to file failed" << std::endl;
        return;
    }

    for (auto p : Data::variables.vars) {
        std::shared_ptr<ISerializable> s = std::visit([](auto&& arg) -> std::shared_ptr<ISerializable> { return arg; }, p.second);
        s->Serialize(f);
    }

    Graph::instance()->Serialize(f);

    f.close();
}

void Serializer::Deserialize(const char* path)
{
    std::cout << "Opening file: " << path << std::endl;

    std::ifstream file(path, std::ios::in);
    if (!file.is_open()) {
        std::cerr << "Could not open file " << path << std::endl;
        return;
    }

    std::vector<std::string> contents;
    std::string line;

    std::string currentContent;
    while (std::getline(file, line)) {
        size_t begin = 0;
        size_t end = line.length();

        RemoveComment(line, begin, end);
        RemoveLeadingSpaces(line, begin, end);

        // ignore empty
        if (begin >= end) continue;

        // add to current content
        currentContent += line.substr(begin, end - begin);

        size_t i = 0;
        int bracketCount = 0;
        for (; i < currentContent.length(); ++i) {
            if (currentContent[i] == '{') {
                bracketCount++;
                if (bracketCount > 1 || bracketCount < 0) {
                    std::cerr << "Error reading content: " << currentContent << std::endl;
                }
            }

            if (currentContent[i] == '}') {
                bracketCount--;
                if (bracketCount > 1 || bracketCount < 0) {
                    std::cerr << "Error reading content: " << currentContent << std::endl;
                }
            }

            if (currentContent[i] == ';' && bracketCount == 0) {
                std::string c = currentContent.substr(0, i + 1);
                contents.push_back(c);
                currentContent = currentContent.substr(i + 1);
                i = 0;
                bracketCount = 0;
            }
        }
    }

    for (const std::string& content : contents) {
        ParseContent(content);
    }

    file.close();
}

void Serializer::RemoveLeadingSpaces(const std::string& line, size_t& begin, size_t& end) const
{
    if (begin >= end) return;
    while (line[begin] == ' ' || line[begin] == '\t') {
        if (begin == std::string::npos)break;
        ++begin;
    }
}

void Serializer::RemoveTrailingSpaces(const std::string& line, size_t& begin, size_t& end) const
{
    if (begin >= end) return;
    while (line[end] == ' ' || line[end] == '\t') {
        --end;
    }
}

void Serializer::RemoveComment(const std::string& line, size_t& begin, size_t& end) const
{
    if (begin >= end) return;
    end = line.find_first_of("#");
}

void Serializer::ParseContent(const std::string& line)
{
    size_t begin = 0;
    size_t end = line.length() - 1;
    RemoveLeadingSpaces(line, begin, end);

    std::string type = "";
    std::string rest = "";
    for (size_t i = begin; i <= end; ++i) {
        if (line[i] == ' ' || line[i] == '\t') {
            type = line.substr(begin, i - begin);
            size_t restBegin = i;
            RemoveLeadingSpaces(line, restBegin, end);
            rest = line.substr(restBegin, end - restBegin);
            break;
        }
    }

    std::string name;
    std::string data;
    begin = 0;
    end = rest.length() - 1;

    size_t separator = rest.find_first_of('=');
    size_t openBracket = rest.find_first_of('{');
    separator = openBracket < separator ? openBracket : separator;

    size_t lEnd = separator - 1;
    size_t rBegin = separator + 1;

    if (lEnd <= end) {
        RemoveTrailingSpaces(rest, begin, lEnd);
        name = rest.substr(begin, lEnd - begin + 1);
        RemoveLeadingSpaces(rest, rBegin, end);
        RemoveTrailingSpaces(rest, rBegin, end);
        data = rest.substr(rBegin, end - rBegin + 1);

        end = data.length() - 1;
        if (end >= 0 && data[end] == '}') {
            data = data.substr(0, end);
        }
    }
    else {
        name = rest;
        data = "";
    }

    if (type == "int") ParseInt(name, data);
    else if (type == "float") ParseFloat(name, data);
    else if (type == "bool") ParseBool(name, data);
    else if (type == "image") ParseImage(name, data);
    else if (type == "group") ParseGroup(name, data);
    else if (type == "renderpass") ParseRenderpass(name, data);
    else if (type == "graph") ParseGraph(name, data);
    else {
        std::cerr << "Error reading line: " << line << std::endl;
    }
}

void Serializer::ParseInt(const std::string& name, const std::string& data)
{
    Int i(name);
    if (data != "") {
        i.SetValue(std::stoi(data));
    }
    Data::variables.Set(name, i);
}

void Serializer::ParseFloat(const std::string& name, const std::string& data)
{
    Float f(name);
    if (data != "") {
        f.SetValue(std::stof(data));
    }
    Data::variables.Set(name, f);
}

void Serializer::ParseBool(const std::string& name, const std::string& data)
{
    Bool b(name);
    if (data != "") {
        if (data == "true") {
            b.SetValue(true);
        }
        else if (data == "false") {
            b.SetValue(false);
        }
        else {
            try {
                float f = std::stof(data);
                b.SetValue(f != 0);
            }
            catch (...) {
                std::cerr << "Error parsing bool: " << data << std::endl;
                return;
            }
        }
    }
    Data::variables.Set(name, b);
}

void Serializer::ParseImage(const std::string& name, const std::string& data)
{
    auto content = ParseVarData(data, ';');

    Image image(name);

    std::optional<int> format;
    if (content.find("format") != content.end()) {
        format = (VkFormat)stoi(content.at("format"));
    }
    image.SetFormat(format);

    std::optional<glm::vec2> size;
    if (content.find("width") != content.end() && content.find("height") != content.end()) {
        int w = stoi(content.at("width"));
        int h = stoi(content.at("height"));
        size = glm::vec2(w, h);
    }
    image.SetSize(size);

    Data::variables.Set(name, image);
}

void Serializer::ParseGroup(const std::string& name, const std::string& data)
{
    Components components(name);
    Data::variables.Set(name, components);
}

void Serializer::ParseRenderpass(const std::string& name, const std::string& data)
{
    auto content = ParseVarData(data, ';');

    std::string& clearColorData = content["clearColorEnabled"];
    std::string& clearDepthData = content["clearDepthEnabled"];
    std::string& isPostprocess = content["isPostprocess"];

    std::shared_ptr<RenderPassData> d = std::make_shared<RenderPassData>();

    bool clearColorEnabled = clearColorData == "true";
    d->SetClearColorEnabled(clearColorEnabled);
    if (clearColorEnabled) {
        std::string& clearColorValueData = content["clearColorValue"];
        std::stringstream ss_color(clearColorValueData);
        glm::vec3 clearColorValue;
        ss_color >> clearColorValue.r;
        ss_color >> clearColorValue.g;
        ss_color >> clearColorValue.b;
        d->SetClearColorValue(clearColorValue);
    }

    bool clearDepthEnabled = clearDepthData == "true";
    d->SetClearDepthEnabled(clearDepthEnabled);
    if (clearDepthEnabled) {
        std::string& clearDepthValueData = content["clearDepthValue"];
        std::stringstream ss_depth(clearDepthValueData);
        float clearDepthValue;
        ss_depth >> clearDepthValue;
        d->SetClearDepthValue(clearDepthValue);
    }

    d->isPostprocess = isPostprocess == "true";

    if (content.find("shader") != content.end()) {
        d->SetShaderName(content.at("shader"));
    }

    uint32_t id;
    std::stringstream ss(name);
    ss >> id;

    d->inputsCount = 1;
    d->outputsCount = 1;

    impl->renderPassesCache[id] = d;
}

void Serializer::ParseGraph(const std::string& name, const std::string& data)
{
    std::string code;
    std::stringstream ss(data);
    std::string edgeDelimiter = "->";

    while (std::getline(ss, code, ';')) {
        size_t pos = code.find(edgeDelimiter);
        if (pos != std::string::npos) {
            std::string edgeFrom = code.substr(0, pos);
            std::string edgeTo = code.substr(pos + edgeDelimiter.length());
            ParseGraphEdge(edgeFrom, edgeTo);
        }
        else {
            ParseGraphNode(code);
        }
    }
}

void Serializer::ParseGraphNode(const std::string& str)
{
    size_t metaStart = str.find('[');
    size_t metaEnd = str.find(']');
    std::string data = str.substr(0, metaStart);
    std::string metadata = str.substr(metaStart+1, metaEnd-metaStart-1);

    auto dataSplit = split(data, ".");

    std::string type = dataSplit[0];
    std::string sid = dataSplit[dataSplit.size() - 1];
    std::stringstream ss(sid);
    uint32_t id;
    ss >> id;

    std::map<std::string, std::string> meta = ParseVarData(metadata, ',');

    int xPos = atoi(meta["x"].c_str());
    int yPos = atoi(meta["y"].c_str());
    glm::vec2 pos(xPos, yPos);

    const std::string& n = dataSplit[0];
    if (n == "S" && dataSplit.size() == 2) {
        impl->addNode<BeginNode>(id, "Start", pos);
    }
    else if (n == "I" && dataSplit.size() == 2) {
        impl->addNode<IfNode>(id, "If", pos);
    }
    else if (n == "W" && dataSplit.size() == 2) {
        impl->addNode<WhileNode>(id, "While", pos);
    }
    else if (n == "SC" && dataSplit.size() == 2) {
        impl->addNode<SwapchainNode>(id, "Swapchain", pos);
    }
    else if (n == "RP" && dataSplit.size() == 2) {
        auto data = impl->renderPassesCache[id];
        data->inputsCount = atoi(meta["in"].c_str());
        data->outputsCount = atoi(meta["out"].c_str());
        if (data->isPostprocess) {
            std::shared_ptr<PostprocessExecNode> node = impl->addNode<PostprocessExecNode>(id, "PostprocessExecution", pos);
            node->data = data;
        }
        else {
            std::shared_ptr<RenderPassExecNode> node = impl->addNode<RenderPassExecNode>(id, "RenderPassExecution", pos);
            node->data = data;
        }
    }
    else if (n == "O") {
        const std::string& operatorCode = dataSplit[1];

        if (dataSplit.size() == 3) {
            if (operatorCode == "and") {
                impl->addNode<AndNode>(id, "And", pos);
            }
            if (operatorCode == "or") {
                impl->addNode<OrNode>(id, "Or", pos);
            }
            if (operatorCode == "not") {
                impl->addNode<NotNode>(id, "Not", pos);
            }
        }

        if (dataSplit.size() == 4) {
            const std::string& variant = dataSplit[2];
            const Slot::DataType i = Slot::DataType::Int;
            const Slot::DataType f = Slot::DataType::Float;
            if (variant == "0") {
                impl->ParseOperatorNode<i, i>(id, operatorCode, pos);
            }
            if (variant == "1") {
                impl->ParseOperatorNode<i, f>(id, operatorCode, pos);
            }
            if (variant == "2") {
                impl->ParseOperatorNode<f, i>(id, operatorCode, pos);
            }
            if (variant == "3") {
                impl->ParseOperatorNode<f, f>(id, operatorCode, pos);
            }
        }
    }
    else if (n == "GET") {
        std::string& varName = dataSplit[1];
        if (Data::variables.IsOfType<Int>(varName)) {
            impl->addNode<GetNode<Int>>(id, varName, pos);
        }
        else if (Data::variables.IsOfType<Float>(varName)) {
            impl->addNode<GetNode<Float>>(id, varName, pos);
        }
        else if (Data::variables.IsOfType<Bool>(varName)) {
            impl->addNode<GetNode<Bool>>(id, varName, pos);
        }
        else if (Data::variables.IsOfType<Image>(varName)) {
            impl->addNode<ImageNode>(id, varName, pos);
        }
        else if (Data::variables.IsOfType<Components>(varName)) {
            impl->addNode<RenderComponentsNode>(id, varName, pos);
        }
    }
    else if (n == "SET") {
        std::string& varName = dataSplit[1];
        if (Data::variables.IsOfType<Int>(varName)) {
            impl->addNode<SetNode<Int>>(id, varName, pos);
        }
        else if (Data::variables.IsOfType<Float>(varName)) {
            impl->addNode<SetNode<Float>>(id, varName, pos);
        }
        else if (Data::variables.IsOfType<Bool>(varName)) {
            impl->addNode<SetNode<Bool>>(id, varName, pos);
        }
    }
    else if (n == "SEL") {
        std::string& type = dataSplit[1];
        if (type == "group") {
            impl->addNode<SelectNode<Slot::DataType::RenderComponents>>(id, "Select", pos);
        }

        if (type == "image") {
            impl->addNode<SelectNode<Slot::DataType::ImageRead>>(id, "Select", pos);
        }

        if (type == "int") {
            impl->addNode<SelectNode<Slot::DataType::Int>>(id, "Select", pos);
        }

        if (type == "float") {
            impl->addNode<SelectNode<Slot::DataType::Float>>(id, "Select", pos);
        }

        if (type == "bool") {
            impl->addNode<SelectNode<Slot::DataType::Bool>>(id, "Select", pos);
        }
    }
}

void Serializer::ParseGraphEdge(const std::string& from, const std::string& to)
{
    std::stringstream fromStream(from);
    std::stringstream toStream(to);
    std::string fromId;
    std::string fromPort;
    std::string toId;
    std::string toPort;
    std::getline(fromStream, fromId, ':');
    std::getline(fromStream, fromPort, ':');
    std::getline(toStream, toId, ':');
    std::getline(toStream, toPort, ':');

    std::stringstream from_ss(fromId);
    uint32_t aId;
    from_ss >> aId;

    std::stringstream to_ss(toId);
    uint32_t bId;
    to_ss >> bId;

    impl->getNode(aId)->GetSlot(fromPort)->ConnectTo(impl->getNode(bId)->GetSlot(toPort));
}

std::map<std::string, std::string> Serializer::ParseVarData(const std::string& data, char delimiter)
{
    std::string line;
    std::stringstream ss(data);

    std::map<std::string, std::string> parsedContent;
    while (std::getline(ss, line, delimiter)) {
        size_t begin = 0;
        size_t end = line.length() - 1;
        RemoveLeadingSpaces(line, begin, end);

        std::string name;
        std::string data;

        size_t separator = line.find_first_of('=');

        size_t lEnd = separator - 1;
        size_t rBegin = separator + 1;

        if (lEnd <= end) {
            RemoveTrailingSpaces(line, begin, lEnd);
            name = line.substr(begin, lEnd - begin + 1);
            RemoveLeadingSpaces(line, rBegin, end);
            RemoveTrailingSpaces(line, rBegin, end);
            data = line.substr(rBegin, end - rBegin + 1);
            parsedContent[name] = data;
        }
    }
    return parsedContent;
}

std::vector<std::string> Serializer::split(const std::string& in, const std::string& delimiter)
{
    size_t pos = 0;
    size_t start = 0;
    std::string token;
    std::vector<std::string> dataSplit;
    while ((pos = in.find(delimiter, start)) != std::string::npos) {
        token = in.substr(start, pos - start);
        dataSplit.push_back(token);
        start = pos + delimiter.length();
    }
    dataSplit.push_back(in.substr(start));
    return dataSplit;
}
