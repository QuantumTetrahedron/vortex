#pragma once

#include <fstream>
#include <vector>
#include <map>
#include "glm/glm.hpp"
#include "Slot.h"

class ISerializable {
public:
	virtual void Serialize(std::ofstream& out) = 0;
};

class Serializer {
public:
	Serializer();
	~Serializer();
	void Serialize(const char* file);
	void Deserialize(const char* file);

private:
	void RemoveLeadingSpaces(const std::string& line, size_t& begin, size_t& end) const;
	void RemoveTrailingSpaces(const std::string& line, size_t& begin, size_t& end) const;
	void RemoveComment(const std::string& line, size_t& begin, size_t& end) const;

	void ParseContent(const std::string& line);
	void ParseInt(const std::string& name, const std::string& data);
	void ParseFloat(const std::string& name, const std::string& data);
	void ParseBool(const std::string& name, const std::string& data);
	void ParseImage(const std::string& name, const std::string& data);
	void ParseGroup(const std::string& name, const std::string& data);
	void ParseRenderpass(const std::string& name, const std::string& data);
	void ParseGraph(const std::string& name, const std::string& data);
	void ParseGraphNode(const std::string& data);
	void ParseGraphEdge(const std::string& from, const std::string& to);
	std::map<std::string, std::string> ParseVarData(const std::string& data, char delimiter);

	std::vector<std::string> split(const std::string& in, const std::string& delimiter);

	struct pImpl;
	pImpl* impl;
};