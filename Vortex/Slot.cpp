#include "Slot.h"
#include "Node.h"
#include "Window.h"
#include "NodesRenderer.h"

Slot::Slot(const std::string& id, const std::string& dispName, Type t, DataType dt, Quantity q, glm::vec2 pos)
	: id(id), displayName(dispName), type(t), dataType(dt), quantity(q), position(pos)
{
}

bool Slot::IsCompatible(std::shared_ptr<Slot> slot)
{
	return slot->type != type && slot->dataType == dataType && slot->parent.lock() != parent.lock();
}

void Slot::ConnectTo(std::shared_ptr<Slot> slot) {
	if (IsCompatible(slot)) {
		if (quantity == Slot::Quantity::One) {
			DisconnectAll();
		}
		if (slot->quantity == Slot::Quantity::One) {
			slot->DisconnectAll();
		}
		slot->connections.push_back(shared_from_this());
		connections.push_back(slot);
	}
}

void Slot::DisconnectAll()
{
	for (auto& s : connections) {
		DisconectFrom(s.lock());
	}
}

void Slot::DisconectFrom(std::shared_ptr<Slot> slot) {
	slot->connections.erase(std::remove_if(slot->connections.begin(), slot->connections.end(), [&](std::weak_ptr<Slot> s) {return s.lock() == shared_from_this(); }));
	connections.erase(std::remove_if(connections.begin(), connections.end(), [slot](std::weak_ptr<Slot> s) {return s.lock() == slot; }));
}

void Slot::Draw(ImDrawList* drawList, glm::vec2 positionOffset)
{
	ndraw::SlotDrawConfig cfg;
	cfg.position = position + positionOffset;
	cfg.radius = dataType == DataType::Exec ? 10.0f : 8.0f;

	ImColor lineColor;
	lineColor = GetColor();

	cfg.backgroundColor = lineColor;
	cfg.backgroundColor.Value.x /= 3.0f;
	cfg.backgroundColor.Value.y /= 3.0f;
	cfg.backgroundColor.Value.z /= 3.0f;

	if (!connections.empty()) {
		cfg.outlineColor = lineColor;
	}
	else {
		cfg.outlineColor = cfg.backgroundColor;
		cfg.outlineColor.Value.x /= 2.0f;
		cfg.outlineColor.Value.y /= 2.0f;
		cfg.outlineColor.Value.z /= 2.0f;
	}

	bool ok = true;
	for (auto& s : Window::shared.selectedSlots) {
		if (!IsCompatible(s)) {
			ok = false;
			break;
		}
	}

	auto t = shared_from_this();
	if (Window::IsHovered(t)) {
		cfg.highlightColor = ok ? ImColor(0.6f, 0.6f, 0.2f) : ImColor(0.9f, 0.2f, 0.2f);
	}
	if (!connections.empty() || Window::IsSelected(t)) {
		cfg.lineColor = lineColor;
	}

	bool showName = displayName.length() > 0;

	cfg.label.show = showName;
	cfg.label.text = displayName;
	cfg.label.size = 14;
	cfg.label.color = ImColor(0.0f, 0.0f, 0.0f);

	float textLength = displayName.length() * 7.0f;
	float textOffset = 13.0f;
	if (dataType == DataType::Exec) textOffset += 2;
	if (type == Type::output) textOffset = -textOffset - textLength;

	cfg.label.offset = textOffset;

	ndraw::DrawSlot(drawList, cfg);
}

void Slot::DrawConnections(ImDrawList* drawList, glm::vec2 positionOffset)
{
	if (type != Slot::Type::output) return;
	glm::vec2 slotPos = GetPosition() + positionOffset;

	for (auto& dest : connections) {
		auto dst = dest.lock();
		if (!dst) continue;
		glm::vec2 destPos = dst->GetPosition() + positionOffset;

		ndraw::ConnectionDrawConfig cfg;
		cfg.from = slotPos;
		cfg.to = destPos;
		cfg.color = GetColor();
		cfg.thickness = dataType == Slot::DataType::Exec ? 5.0f : 3.0f;

		ndraw::DrawConnection(drawList, cfg);
	}
}

ImColor Slot::GetColor()
{
	if (dataType == DataType::Exec) {
		return ImColor(0.9f, 0.9f, 0.9f);
	}
	if (dataType == DataType::ImageWrite || dataType == DataType::ImageRead) {
		return ImColor(0.2f, 0.8f, 0.8f);
	}
	if (dataType == DataType::RenderComponents) {
		return ImColor(0.8f, 0.2f, 0.2f);
	}
	if (dataType == DataType::Int) {
		return ImColor(0.2f, 0.8f, 0.2f);
	}
	if (dataType == DataType::Float) {
		return ImColor(0.8f, 0.8f, 0.2f);
	}
	if (dataType == DataType::Bool) {
		return ImColor(0.4f, 0.1f, 0.8f);
	}
}

Slot::Type Slot::GetType() const
{
	return type;
}

Slot::DataType Slot::GetDataType() const
{
	return dataType;
}

void Slot::SetParent(std::shared_ptr<Node> p)
{
	parent = p;
}

std::weak_ptr<Node> Slot::GetParent() const
{
	return parent;
}

glm::vec2 Slot::GetPosition(bool world)
{
	glm::vec2 ret = position;
	if (world) ret += parent.lock()->position;
	return ret;
}

std::string Slot::GetName() const
{
	return id;
}
