#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <map>
#include <string>
#include <memory>

#include "ImGUI/imgui.h"
#include "ImGUI/imgui_impl_glfw.h"
#include "ImGUI/imgui_impl_vulkan.h"
#include "ImGUI/imgui_internal.h"

class Node;

class Slot : public std::enable_shared_from_this<Slot>
{
public:
	enum class Type {
		input, output
	};
	enum class DataType {
		Exec, ImageWrite, ImageRead, RenderComponents,
		Int, Float, Bool
	};
	enum class Quantity {
		One, Many
	};

	Slot(const std::string& id, const std::string& dispName, Type t, DataType dt, Quantity q, glm::vec2 pos);
	bool IsCompatible(std::shared_ptr<Slot> slot);

	void ConnectTo(std::shared_ptr<Slot> slot);
	void DisconnectAll();
	void DisconectFrom(std::shared_ptr<Slot> slot);
	void Draw(ImDrawList* drawList, glm::vec2 positionOffset);
	void DrawConnections(ImDrawList* drawList, glm::vec2 positionOffset);

	ImColor GetColor();
	Type GetType() const;
	DataType GetDataType() const;

	void SetParent(std::shared_ptr<Node> p);
	std::weak_ptr<Node> GetParent() const;
	glm::vec2 GetPosition(bool world = true);

	std::vector<std::weak_ptr<Slot>> connections;

	std::string GetName() const;
private:
	std::weak_ptr<Node> parent;
	glm::vec2 position;

	Type type;
	DataType dataType;
	Quantity quantity;
	std::string id;
	std::string displayName;
};

