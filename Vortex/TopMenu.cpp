#include "TopMenu.h"
#include <iostream>
#include "Log.h"

void TopMenu::Draw()
{
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            if (ImGui::MenuItem("Save")) {
                saveBrowser.Open();
            }
            if (ImGui::MenuItem("Open")) {
                openBrowser.Open();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }

    saveBrowser.Display();
    openBrowser.Display();

    if (saveBrowser.HasSelected())
    {
        serializer.Serialize(saveBrowser.GetSelected().string().c_str());
        saveBrowser.ClearSelected();
    }

    if (openBrowser.HasSelected())
    {
        Graph::instance()->Clear();
        Data::variables.RemoveAll();
        Log::Clear();

        std::string selected = openBrowser.GetSelected().string();
        serializer.Deserialize(selected.c_str());
        std::string info = "Loaded graph: " + selected;
        Log::Info(info);
        openBrowser.ClearSelected();
    }
}
