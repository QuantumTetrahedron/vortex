#pragma once
#include "ImGUI/imgui.h"
#include "imfilebrowser.h"
#include "Serializer.h"
#include "Workspace.h"

class TopMenu {
public:
	void Draw();

private:
	ImGui::FileBrowser saveBrowser = ImGui::FileBrowser(ImGuiFileBrowserFlags_EnterNewFilename | ImGuiFileBrowserFlags_CreateNewDir);
	ImGui::FileBrowser openBrowser;

	Serializer serializer;
};