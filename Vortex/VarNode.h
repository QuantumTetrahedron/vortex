#pragma once
#include "Node.h"
#include "Data.h"
#include "NodesRenderer.h"

class VarNode : public Node {
public:
	virtual bool DependsOn(const std::string& varName) const {
		return varName == variableName;
	}
	std::string variableName;
};

template<typename T>
Slot::DataType GetDataType() {
	if (std::is_same_v<T, Int>) {
		return Slot::DataType::Int;
	}
	else if (std::is_same_v<T, Float>) {
		return Slot::DataType::Float;
	}
	else if (std::is_same_v<T, Bool>) {
		return Slot::DataType::Bool;
	}
}

template<typename T>
class GetNode : public VarNode {
public:
	GetNode(const std::string& _name) {
		id = _name;
		value = Data::variables.Get<T>(_name);
		variableName = _name;
		slotOffset = 0.0f;
		position = glm::vec2(0.0f, 0.0f);
		size = glm::vec2(120.0f, 30.0f);
		name = "GET." + _name;
		AddSlot("out", "", Slot::Type::output, GetDataType<T>(), Slot::Quantity::Many);
		headerColor = GetSlot("out")->GetColor();
	}

	void OnClick() override {
		Properties::Display(value);
	}

	void Draw(ImDrawList* drawList, glm::vec2 positionOffset) override {
		ndraw::NodeDrawConfig cfg;
		cfg.position = position + positionOffset;
		cfg.size = size;
		cfg.backgroundColor = headerColor;
		cfg.borderColor = ImColor(0.0f, 0.0f, 0.0f);

		auto t = shared_from_this();
		if (Window::IsSelected(t)) {
			cfg.highlightColor = ImColor(0.8f, 0.8f, 0.4f);
		}
		else if (Window::IsHovered(t)) {
			cfg.highlightColor = ImColor(0.4f, 0.4f, 0.8f);
		}

		cfg.label.color = ImColor(0.0f, 0.0f, 0.0f);
		cfg.label.size = 18;
		cfg.label.position = position + positionOffset + glm::vec2(5.0f);
		cfg.label.text = id;

		ndraw::DrawNode(drawList, cfg);

		for (auto& s : slots) {
			s.second->Draw(drawList, positionOffset + position);
		}
	}

	std::shared_ptr<T> value;
};

template<typename T>
class SetNode : public VarNode {
public:
	SetNode(const std::string& _name) {
		id = _name;
		value = Data::variables.Get<T>(_name);
		variableName = _name;
		position = glm::vec2(0.0f, 0.0f);
		size = glm::vec2(120.0f, 80.0f);
		name = "SET." + _name;
		AddSlot("prev", "", Slot::Type::input, Slot::DataType::Exec, Slot::Quantity::Many);
		AddSlot("next", "", Slot::Type::output, Slot::DataType::Exec, Slot::Quantity::One);
		AddSlot("in", "", Slot::Type::input, GetDataType<T>(), Slot::Quantity::One);
		AddSlot("out", "", Slot::Type::output, GetDataType<T>(), Slot::Quantity::Many);
		headerColor = GetSlot("in")->GetColor();
	}

	void OnClick() override {
		Properties::Display(value);
	}

	std::shared_ptr<T> value;
};