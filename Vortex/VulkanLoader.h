#pragma once

#include "ImGUI/imgui.h"
#include "ImGUI/imgui_impl_glfw.h"
#include "ImGUI/imgui_impl_vulkan.h"

#include <stdio.h>
#include <stdlib.h>

#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

VKAPI_ATTR VkBool32 VKAPI_CALL DebugReport(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objectType, uint64_t object, size_t location, int32_t messageCode, const char* pLayerPrefix, const char* pMessage, void* pUserData);
void CheckVkResult(VkResult err);

class VulkanLoader
{
public:
	void Setup(GLFWwindow* window, const char** extensions, uint32_t extensionsCount);

	void InitImGui(GLFWwindow* window);

	void LoadFonts();

	void NewFrame(GLFWwindow* window);

	void Render();

	void Cleanup();

private:
	void CreateVulkanInstance(const char** extensions, uint32_t extensionsCount);
	void SelectGPU();
	void SelectGraphicsQueueFamily();
	void CreateLogicalDevice();
	void CreateDescriptorPool();

	void RenderFrame(ImDrawData* draw_data);
	void PresentFrame();

	void SetupVulkanWindow(ImGui_ImplVulkanH_Window* wd, VkSurfaceKHR surface, int width, int height);
	void CleanupVulkanWindow();

	VkAllocationCallbacks* allocator = nullptr;
	VkInstance instance = VK_NULL_HANDLE;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	VkDevice device = VK_NULL_HANDLE;

	uint32_t queueFamily = (uint32_t)-1;
	VkQueue queue = VK_NULL_HANDLE;

	VkDebugReportCallbackEXT debugReport = VK_NULL_HANDLE;
	
	VkPipelineCache pipelineCache = VK_NULL_HANDLE;
	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;

	ImGui_ImplVulkanH_Window mainWindowData;

	int minImageCount = 2;
	bool swapchainRebuild = false;

#ifdef _DEBUG
	static const bool debug = true;
#else
	static const bool debug = false;
#endif
};

