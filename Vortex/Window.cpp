#include "Window.h"

Window::Shared Window::shared;

bool Window::IsSelected(std::shared_ptr<Node> node)
{
	return shared.selectedNode == node;
}

bool Window::IsSelected(std::shared_ptr<Slot> slot)
{
	return std::find(shared.selectedSlots.begin(), shared.selectedSlots.end(), slot) != shared.selectedSlots.end();
}

bool Window::IsHovered(std::shared_ptr<Node> node)
{
	return shared.hoveredNode == node;
}

bool Window::IsHovered(std::shared_ptr<Slot> slot)
{
	return shared.hoveredSlot == slot;
}