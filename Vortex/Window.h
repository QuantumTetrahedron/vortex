#pragma once

#include <memory>
#include <vector>

#include "Node.h"

class Window
{
public:
	virtual void Draw() = 0;
	virtual void ProcessIO(ImGuiIO& io) = 0;
	virtual ~Window() {}

	static bool IsSelected(std::shared_ptr<Node> node);
	static bool IsSelected(std::shared_ptr<Slot> slot);
	static bool IsHovered(std::shared_ptr<Node> node);
	static bool IsHovered(std::shared_ptr<Slot> slot);

	struct Shared {
		std::shared_ptr<Node> selectedNode;
		std::shared_ptr<Node> hoveredNode;
		std::vector<std::shared_ptr<Slot>> selectedSlots;
		std::shared_ptr<Slot> hoveredSlot;
	};
	static Shared shared;
};

