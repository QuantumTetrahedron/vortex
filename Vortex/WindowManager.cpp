#include "WindowManager.h"

#include "Workspace.h"
#include "Properties.h"
#include "Data.h"
#include "Log.h"
#include <iostream>

std::vector<std::shared_ptr<Window>> WindowManager::windows;
TopMenu WindowManager::topMenu;

void WindowManager::Init()
{
    windows.push_back(std::make_shared<Workspace>());
    windows.push_back(std::make_shared<Properties>());
    windows.push_back(std::make_shared<Data>());
    windows.push_back(std::make_shared<Log>());
}

void WindowManager::Draw()
{
    topMenu.Draw();

    ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->WorkPos);
    ImGui::SetNextWindowSize(viewport->WorkSize);
    ImGui::SetNextWindowViewport(viewport->ID);

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

    ImGui::Begin("Docker", nullptr, window_flags);

    ImGuiID dockspaceID = ImGui::GetID("MyDockSpace");

    if (!ImGui::DockBuilderGetNode(dockspaceID)) {
        ImGui::DockBuilderRemoveNode(dockspaceID);
        ImGui::DockBuilderAddNode(dockspaceID, ImGuiDockNodeFlags_DockSpace);
        ImGui::DockBuilderSetNodeSize(dockspaceID, ImVec2(800, 800));

        ImGuiID dock_main_id = dockspaceID;

        ImGuiID dock_left_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Left, 0.15f, nullptr, &dock_main_id);
        ImGuiID dock_right_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Right, 0.25f, nullptr, &dock_main_id);
        ImGuiID dock_down_id = ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Down, 0.2f, nullptr, &dock_main_id);

        ImGui::DockBuilderDockWindow("Data", dock_left_id);
        ImGui::DockBuilderDockWindow("Properties", dock_right_id);
        ImGui::DockBuilderDockWindow("Log", dock_down_id);

        ImGui::DockBuilderFinish(dock_main_id);
    }
    ImGui::DockSpace(dockspaceID, ImVec2(0, 0), ImGuiDockNodeFlags_NoWindowMenuButton | ImGuiDockNodeFlags_NoCloseButton);

    int dockedFlags = ImGuiWindowFlags_NoMove;

    ImGui::SetNextWindowDockID(dockspaceID, ImGuiCond_Appearing);

    for (auto& w : windows) {
        w->Draw();
    }

    ImGui::End();
    ImGui::PopStyleVar(3);
}

void WindowManager::ProcessIO(ImGuiIO& io)
{
    for (auto& w : windows) {
        w->ProcessIO(io);
    }
}

