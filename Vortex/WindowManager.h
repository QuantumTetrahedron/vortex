#pragma once

#include <vector>
#include <memory>
#include "Window.h"
#include "TopMenu.h"

class WindowManager
{
public:
	WindowManager() = delete;

	static void Init();

	static void Draw();
	static void ProcessIO(ImGuiIO& io);

private:
	static std::vector<std::shared_ptr<Window>> windows;
	static TopMenu topMenu;
};

