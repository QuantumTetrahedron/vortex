#include "Workspace.h"
#include <algorithm>
#include <iostream>

#include "RenderPassExecNode.h"
#include "ImageNode.h"
#include "RenderComponentsNode.h"
#include "FlowControl.h"
#include "Operators.h"
#include "VarNode.h"
#include "Log.h"

#include "WindowManager.h"

const int DELETE_KEY_CODE = 261;

Workspace::Workspace() {
    workspaceOffset = glm::vec2(200, 200);
    Graph::instance()->AddNode<BeginNode>("Start", glm::vec2(200, 200));

    std::function<void(const std::string&)> f = [&](const std::string& s) {
        OnVariableRemove(s);
    };

    Data::variables.AddRemoveListener(f);
}

void Workspace::Draw() {
    ImGui::Begin("Workspace", nullptr, ImGuiWindowFlags_NoMove);

    ImDrawList* draw_list = ImGui::GetWindowDrawList();
    static float sz = 36.0f;

    static ImVec4 colf = ImVec4(1.0f, 1.0f, 0.4f, 1.0f);
    const ImU32 col = ImColor(colf);
    static float thickness = 3.0f;

    auto& nodes = Graph::instance()->GetNodes();

    for (auto it = nodes.rbegin(); it != nodes.rend(); ++it) {
        auto node = *it;
        node->Draw(draw_list, workspaceOffset);
    }

    for (auto it = nodes.rbegin(); it != nodes.rend(); it++) {
        auto node = *it;
        node->DrawConnections(draw_list, workspaceOffset);
    }

    for (auto& s : shared.selectedSlots) {
        ndraw::ConnectionDrawConfig cfg;
        
        if (s->GetType() == Slot::Type::output) {
            cfg.from = s->GetPosition() + workspaceOffset;
            cfg.to = mousePos + workspaceOffset;
        }
        else {
            cfg.from = mousePos + workspaceOffset;
            cfg.to = s->GetPosition() + workspaceOffset;
        }

        cfg.color = s->GetColor();
        cfg.thickness = s->GetDataType() == Slot::DataType::Exec ? 5.0f : 3.0f;
        ndraw::DrawConnection(draw_list, cfg);
    }

    hovered = ImGui::IsWindowHovered();

    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 1.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8.0f, 8.0f));

    if (ImGui::IsWindowHovered() && ImGui::IsMouseReleased(ImGuiMouseButton_Right) && shouldOpenContextMenu) {
        ImGui::OpenPopup("contextMenu");
        shouldOpenContextMenu = false;
    }
    if (ImGui::BeginPopup("contextMenu")) {

        // Main

        if (ImGui::MenuItem("RenderPass Execution Node")) {
            Graph::instance()->AddNode<RenderPassExecNode>("RenderPassExecution", mousePos);
        }

        if (ImGui::MenuItem("Postprocess Execution Node")) {
            Graph::instance()->AddNode<PostprocessExecNode>("PostprocessExecution", mousePos);
        }

        if (ImGui::MenuItem("Swapchain Node")) {
            Graph::instance()->AddNode<SwapchainNode>("Swapchain", mousePos);
        }
        
        DrawVariablesMenu<Components, RenderComponentsNode>("Components");
        DrawVariablesMenu<Image, ImageNode>("Images");
        DrawVariablesMenu<Int>("Ints");
        DrawVariablesMenu<Float>("Floats");
        DrawVariablesMenu<Bool>("Bools");

        // Flow Control

        if (ImGui::BeginMenu("Flow Control")) {

            if (ImGui::MenuItem("If")) {
                Graph::instance()->AddNode<IfNode>("If", mousePos);
            }

            if (ImGui::MenuItem("While")) {
                Graph::instance()->AddNode<WhileNode>("While", mousePos);
            }

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Select")) {
            if (ImGui::MenuItem("components")) {
                Graph::instance()->AddNode<SelectNode<Slot::DataType::RenderComponents>>("Select", mousePos);
            }

            if (ImGui::MenuItem("image")) {
                Graph::instance()->AddNode<SelectNode<Slot::DataType::ImageRead>>("Select", mousePos);
            }

            if (ImGui::MenuItem("int")) {
                Graph::instance()->AddNode<SelectNode<Slot::DataType::Int>>("Select", mousePos);
            }

            if (ImGui::MenuItem("float")) {
                Graph::instance()->AddNode<SelectNode<Slot::DataType::Float>>("Select", mousePos);
            }

            if (ImGui::MenuItem("bool")) {
                Graph::instance()->AddNode<SelectNode<Slot::DataType::Bool>>("Select", mousePos);
            }

            ImGui::EndMenu();
        }

        // Operators

        if (ImGui::BeginMenu("Operators")) {
            OperatorVariantsMenu<EqualNode>("==");
            OperatorVariantsMenu<NotEqualNode>("!=");
            OperatorVariantsMenu<GreaterNode>(">");
            OperatorVariantsMenu<GreaterEqualNode>(">=");
            OperatorVariantsMenu<LesserNode>("<");
            OperatorVariantsMenu<LesserEqualNode>("<=");
            OperatorVariantsMenu<AddNode>("+");
            OperatorVariantsMenu<SubtractNode>("-");
            OperatorVariantsMenu<MultiplyNode>("*");
            OperatorVariantsMenu<DivideNode>("/");

            if (ImGui::MenuItem("And")) {
                Graph::instance()->AddNode<AndNode>("And", mousePos);
            }
            if (ImGui::MenuItem("Or")) {
                Graph::instance()->AddNode<OrNode>("Or", mousePos);
            }
            if (ImGui::MenuItem("Not")) {
                Graph::instance()->AddNode<NotNode>("Not", mousePos);
            }

            ImGui::EndMenu();
        }

        ImGui::EndPopup();
    }

    ImGui::PopStyleVar(2);
    ImGui::End();
}

void Workspace::ProcessIO(ImGuiIO& io) {
    if (!hovered) {
        shared.selectedSlots.clear();
        state = State::idle;
        return;
    }
    mousePos = glm::vec2(io.MousePos.x, io.MousePos.y) - workspaceOffset;

    shared.hoveredNode = nullptr;
    for (auto& node : Graph::instance()->GetNodes()) {
        if (node->MouseOverNode(mousePos)) {
            shared.hoveredNode = node;
            shared.hoveredSlot = node->MouseOverSlot(mousePos);
            break;
        }
    }
    if (!shared.hoveredNode) shared.hoveredSlot = nullptr;

    switch (state) {
    case State::idle:
        IdleIO(io);
        break;
    case State::viewDrag:
        ViewDragIO(io);
        break;
    case State::nodeDrag:
        NodeDragIO(io);
        break;
    case State::lineDrag:
        LineDragIO(io);
        break;
    default:
        break;
    }
}

void Workspace::IdleIO(ImGuiIO& io)
{
    if (io.MouseClicked[0]) {
        if (shared.hoveredNode) {
            shared.selectedNode = shared.hoveredNode;
            if (shared.hoveredSlot) {
                shared.selectedSlots.push_back(shared.hoveredSlot);
                state = State::lineDrag;
            }
            else {
                dragOffset = mousePos - shared.selectedNode->position;
                state = State::nodeDrag;
            }
            BringSelectedToFront();
            shared.selectedNode->OnClick();
        }
    }

    if (io.MouseClicked[1]) {
        if (shared.hoveredSlot && !shared.hoveredSlot->connections.empty()) {
            std::transform(shared.hoveredSlot->connections.begin(), shared.hoveredSlot->connections.end(), std::back_inserter(shared.selectedSlots), [](std::weak_ptr<Slot> s) {return s.lock(); });
            for (auto& s : shared.selectedSlots) {
                s->DisconectFrom(shared.hoveredSlot);
            }
            state = State::lineDrag;
            BringSelectedToFront();
        }

        if (!shared.hoveredNode && !shared.hoveredSlot) {
            shouldOpenContextMenu = true;
        }
    }

    if (io.MouseClicked[2]) {
        glm::vec2 clickPos = glm::vec2(io.MouseClickedPos[2].x, io.MouseClickedPos[2].y);
        state = State::viewDrag;
        dragOffset = clickPos - workspaceOffset;
    }

    static bool press = false;
    if (io.KeysDown[DELETE_KEY_CODE] && !press && shared.selectedNode) {
        if (shared.selectedNode->canBeDeleted) {
            Graph::instance()->RemoveNode(shared.selectedNode);
            shared.selectedNode = nullptr;
        }
        press = true;
    }
    else if (!io.KeysDown[DELETE_KEY_CODE] && press) {
        press = false;
    }
}

void Workspace::ViewDragIO(ImGuiIO& io)
{
    glm::vec2 mousePos = glm::vec2(io.MousePos.x, io.MousePos.y);
    workspaceOffset = mousePos - dragOffset;

    if (io.MouseReleased[2]) {
        state = State::idle;
    }
}

void Workspace::NodeDragIO(ImGuiIO& io)
{
    glm::vec2 mousePos = glm::vec2(io.MousePos.x, io.MousePos.y) - workspaceOffset;
    shared.selectedNode->position = mousePos - dragOffset;

    if (io.MouseReleased[0]) {
        state = State::idle;
    }
}

void Workspace::LineDragIO(ImGuiIO& io)
{
    if (io.MouseReleased[0] || io.MouseReleased[1]) {
        if (shared.hoveredSlot) {
            for (auto& s : shared.selectedSlots) {
                if (shared.hoveredSlot != s) {
                    s->ConnectTo(shared.hoveredSlot);
                }
            }
        }
        shared.selectedSlots.clear();
        state = State::idle;
    }
}

void Workspace::OnVariableRemove(const std::string& name)
{
    std::set<std::shared_ptr<Node>> toRemove;
    for (auto& n : Graph::instance()->GetNodes()) {
        std::shared_ptr<VarNode> node = std::dynamic_pointer_cast<VarNode>(n);
        if (node && node->DependsOn(name)) {
            toRemove.insert(n);
        }
    }
    Log::Info("Removing " + std::to_string(toRemove.size()) + " nodes");
    Graph::instance()->RemoveNodes(toRemove);
}

void Workspace::BringSelectedToFront() {
    auto& nodes = Graph::instance()->GetNodes();
    auto pivot = std::find_if(nodes.begin(), nodes.end(), [](std::shared_ptr<Node> n) { return n == shared.selectedNode; });
    if (pivot != nodes.end()) {
        std::rotate(nodes.begin(), pivot, pivot + 1);
    }
}