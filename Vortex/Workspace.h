#pragma once
#include "Window.h"
#include "Graph.h"

class Workspace : public Window
{
public:
	Workspace();
	void Draw() override;
	void ProcessIO(ImGuiIO& io) override;

private:
	glm::vec2 workspaceOffset;

	glm::vec2 dragOffset = glm::vec2(0.0f);
	glm::vec2 mousePos = glm::vec2(0.0f);
	bool hovered = false;
	bool shouldOpenContextMenu = false;

	void BringSelectedToFront();

	enum class State {
		idle, viewDrag, nodeDrag, lineDrag
	};
	State state = State::idle;

	void IdleIO(ImGuiIO& io);
	void ViewDragIO(ImGuiIO& io);
	void NodeDragIO(ImGuiIO& io);
	void LineDragIO(ImGuiIO& io);

	template<typename VarT, typename NodeT> requires IsNode<NodeT>
	void DrawVariablesMenu(const std::string& menuName) {
		if (ImGui::BeginMenu(menuName.c_str(), !Data::variables.Empty<VarT>())) {
			for (auto& p : Data::variables.vars) {
				if (std::holds_alternative<std::shared_ptr<VarT>>(p.second)) {
					auto& name = p.first;
					if (ImGui::MenuItem(name.c_str())) {
						Graph::instance()->AddNode<NodeT>(name, mousePos);
					}
				}
			}
			ImGui::EndMenu();
		}
	}

	template<typename VarT>
	void DrawVariablesMenu(const std::string& menuName) {
		if (ImGui::BeginMenu(menuName.c_str(), !Data::variables.Empty<VarT>())) {
			for (auto& p : Data::variables.vars) {
				if (std::holds_alternative<std::shared_ptr<VarT>>(p.second)) {
					auto& name = p.first;
					if (ImGui::BeginMenu(name.c_str())) {
						std::string getId = "Get##get-" + name;
						std::string setId = "Set##set-" + name;
						if (ImGui::MenuItem(getId.c_str())) {
							Graph::instance()->AddNode<GetNode<VarT>>(name, mousePos);
						}
						if (ImGui::MenuItem(setId.c_str())) {
							Graph::instance()->AddNode<SetNode<VarT>>(name, mousePos);
						}

						ImGui::EndMenu();
					}
				}
			}
			ImGui::EndMenu();
		}
	}

	template<template<Slot::DataType, Slot::DataType> class T>
	void OperatorVariantsMenu(const std::string& name) {
		const Slot::DataType i = Slot::DataType::Int;
		const Slot::DataType f = Slot::DataType::Float;
		const std::string iStr = "int", fStr = "float", space = " ";
		if (ImGui::BeginMenu(name.c_str())) {
			if (ImGui::MenuItem((iStr + space + name + space + iStr).c_str())) {
				Graph::instance()->AddNode<T<i, i>>(name.c_str(), mousePos);
			}
			if (ImGui::MenuItem((iStr + space + name + space + fStr).c_str())) {
				Graph::instance()->AddNode<T<i, f>>(name.c_str(), mousePos);
			}
			if (ImGui::MenuItem((fStr + space + name + space + iStr).c_str())) {
				Graph::instance()->AddNode<T<f, i>>(name.c_str(), mousePos);
			}
			if (ImGui::MenuItem((fStr + space + name + space + fStr).c_str())) {
				Graph::instance()->AddNode<T<f, f>>(name.c_str(), mousePos);
			}
			ImGui::EndMenu();
		}
	}

	void OnVariableRemove(const std::string& name);
};

