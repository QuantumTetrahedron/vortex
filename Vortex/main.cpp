#include "VulkanLoader.h"
#include "WindowManager.h"

const unsigned int width = 1600;
const unsigned int height = 900;

void glfwErrorCallback(int error, const char* description);

int main() {
    glfwSetErrorCallback(glfwErrorCallback);
    if (!glfwInit())
        return 1;

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); 
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    GLFWwindow* window = glfwCreateWindow(width, height, "Vortex", nullptr, nullptr);
    
    if (!glfwVulkanSupported())
    {
        printf("GLFW: Vulkan Not Supported\n");
        return 1;
    }
    uint32_t extensions_count = 0;
    const char** extensions = glfwGetRequiredInstanceExtensions(&extensions_count);
    VulkanLoader vk;
    vk.Setup(window, extensions, extensions_count);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

    ImGui::StyleColorsDark();
    vk.InitImGui(window);

    vk.LoadFonts();

    WindowManager::Init();

    vk::Init();

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        vk.NewFrame(window);

        WindowManager::ProcessIO(io);
        WindowManager::Draw();

        vk.Render();
    }

    vk.Cleanup();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}


void glfwErrorCallback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}